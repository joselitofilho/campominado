/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * JFramePrincipal.java
 *
 * Created on 09/07/2013, 23:04:10
 */

package campominado;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author joselito
 */
public class JFramePrincipal extends javax.swing.JFrame {

    public static final int LINHAS = 16;
    public static final int COLUNAS = 16;
    public static final int BOMBAS = 51;

    public static final int CODIGO_VAZIO = 0;
    public static final int CODIGO_BOMBA = 99;

    private int[][] campoMinado;
    private boolean[][] visitadoNaRecursao;
    private JButton[][] buttons;
    private int totalBombasJogo;
    private int totalBombasP1;
    private int totalBombasP2;


    private ImageIcon bombaImage;
    private ImageIcon bandeiraImage;
    private int jogadorDaVez;

    /** Creates new form JFramePrincipal */
    public JFramePrincipal() {
        initComponents();

        Toolkit tk = Toolkit.getDefaultToolkit();
        int xSize = ((int) tk.getScreenSize().getWidth()) - 60;
        int ySize = ((int) tk.getScreenSize().getHeight()) - 60;
        this.setSize(xSize,ySize);

        Image image = new ImageIcon("imagens/bomba.png").getImage();
        Image newImage = image.getScaledInstance(16, 16, java.awt.Image.SCALE_SMOOTH);
        this.bombaImage = new ImageIcon(newImage);

        Image imageBandeira = new ImageIcon("imagens/bandeira.png").getImage();
        Image newImageBandeira = imageBandeira.getScaledInstance(16, 16, java.awt.Image.SCALE_SMOOTH);
        this.bandeiraImage = new ImageIcon(newImageBandeira);

        jLabelBandeiraP1.setIcon(this.bandeiraImage);
        jLabelBandeiraP2.setIcon(this.bandeiraImage);

        jLabelVencedor.setVisible(false);

        this.jLabelQuantasFaltam.setText("" + BOMBAS);
        this.totalBombasJogo = BOMBAS;
        this.totalBombasP1 = 0;
        this.totalBombasP2 = 0;
        this.visitadoNaRecursao = new boolean[LINHAS][COLUNAS];

        iniciarCampoMinado();

        iniciarBotoes();

        //printCampoMinado();

        this.jogadorDaVez = 1;
        vezDoJogador(this.jogadorDaVez);
    }

    private void iniciarCampoMinado() {
        this.campoMinado = new int[LINHAS][COLUNAS];
        for (int i=0; i < LINHAS; i++) {
            for (int j=0; j < COLUNAS; j++) {
                this.campoMinado[i][j] = CODIGO_VAZIO;
            }
        }

        int bombasRestantes = BOMBAS;

        Random randomLinha = new Random();
        Random randomColuna = new Random();
        while (bombasRestantes > 0) {
            int linha = randomLinha.nextInt(16);
            int coluna = randomColuna.nextInt(16);

            if (this.campoMinado[linha][coluna] != CODIGO_BOMBA) {
                this.campoMinado[linha][coluna] = CODIGO_BOMBA;

                if (linha-1 >= 0 && coluna-1 >= 0
                        && this.campoMinado[linha-1][coluna-1] != CODIGO_BOMBA) ++this.campoMinado[linha-1][coluna-1];
                if (linha-1 >= 0
                        && this.campoMinado[linha-1][coluna] != CODIGO_BOMBA) ++this.campoMinado[linha-1][coluna];
                if (linha-1 >= 0 && coluna+1 < COLUNAS
                        && this.campoMinado[linha-1][coluna+1] != CODIGO_BOMBA) ++this.campoMinado[linha-1][coluna+1];

                if (coluna+1 < COLUNAS
                        && this.campoMinado[linha][coluna+1] != CODIGO_BOMBA) ++this.campoMinado[linha][coluna+1];

                if (linha+1 < LINHAS && coluna+1 < COLUNAS
                        && this.campoMinado[linha+1][coluna+1] != CODIGO_BOMBA) ++this.campoMinado[linha+1][coluna+1];
                if (linha+1 < LINHAS
                        && this.campoMinado[linha+1][coluna] != CODIGO_BOMBA) ++this.campoMinado[linha+1][coluna];
                if (linha+1 < LINHAS && coluna-1 >= 0
                        && this.campoMinado[linha+1][coluna-1] != CODIGO_BOMBA) ++this.campoMinado[linha+1][coluna-1];

                if (coluna-1 >= 0
                        && this.campoMinado[linha][coluna-1] != CODIGO_BOMBA) ++this.campoMinado[linha][coluna-1];

                --bombasRestantes;
            }
        }
    }

    private void iniciarBotoes() {
        this.buttons = new JButton[LINHAS][COLUNAS];

        for (int i = 1; i <= LINHAS; i++) {
            for (int j = 1; j <= COLUNAS; j++) {
                JButton button = new JButton("");
                button.setName(i+":"+j);
                button.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        if (alguemVenceu()) {
                            int jogadorVencedor = quemVenceu();
                            if (jogadorVencedor == 1) {
                                jLabelVencedor.setText("Já temos um vencedor!!! Parabéns " + jTextFieldNomeP1.getText().trim());
                            } else {
                                jLabelVencedor.setText("Já temos um vencedor!!! Parabéns " + jTextFieldNomeP2.getText().trim());
                            }

                            jLabelVencedor.setVisible(true);
                        } else {
                            JButton button = (JButton) e.getSource();
                            String buttonName = button.getName();
                            int linha = Integer.parseInt(buttonName.split(":")[0]) - 1;
                            int coluna = Integer.parseInt(buttonName.split(":")[1]) - 1;

                            if (button.isEnabled()) {
                                buttonActionPerformed(button, linha, coluna);

                                if (campoMinado[linha][coluna] != CODIGO_BOMBA) {
                                    vezDoProximoJogador();
                                }
                            }
                        }
                    }
                });

                jPanel2.add(button);
                this.buttons[i-1][j-1] = button;
            }
        }
    }

    private void buttonActionPerformed(JButton button, int linha, int coluna) {
        if (!this.visitadoNaRecursao[linha][coluna]) {

            if (this.campoMinado[linha][coluna] == CODIGO_VAZIO) {
                button.setText("" + this.campoMinado[linha][coluna]);
                this.visitadoNaRecursao[linha][coluna] = true;
                exibirCamposVizinhos(linha, coluna);
            } else if(this.campoMinado[linha][coluna] == CODIGO_BOMBA) {
                button.setIcon(this.bombaImage);
                if (this.jogadorDaVez == 1) {
                    button.setBackground(Color.blue);
                    this.jLabelBombasP1.setText("" + (++this.totalBombasP1));
                } else {
                    button.setBackground(Color.red);
                    this.jLabelBombasP2.setText("" + (++this.totalBombasP2));
                }
                this.jLabelQuantasFaltam.setText("" + (--this.totalBombasJogo));
            } else {
                button.setText("" + this.campoMinado[linha][coluna]);
            }
            button.setEnabled(false);
        }
    }

    private void exibirCamposVizinhos(int linha, int coluna) {
        if (linha-1 >= 0 && coluna-1 >= 0
                && this.campoMinado[linha-1][coluna-1] != CODIGO_BOMBA) buttonActionPerformed(this.buttons[linha-1][coluna-1], linha-1, coluna-1);
        if (linha-1 >= 0
                && this.campoMinado[linha-1][coluna] != CODIGO_BOMBA) buttonActionPerformed(this.buttons[linha-1][coluna], linha-1, coluna);
        if (linha-1 >= 0 && coluna+1 < COLUNAS
                && this.campoMinado[linha-1][coluna+1] != CODIGO_BOMBA) buttonActionPerformed(this.buttons[linha-1][coluna+1], linha-1, coluna+1);

        if (coluna+1 < COLUNAS
                && this.campoMinado[linha][coluna+1] != CODIGO_BOMBA) buttonActionPerformed(this.buttons[linha][coluna+1], linha, coluna+1);

        if (linha+1 < LINHAS && coluna+1 < COLUNAS
                && this.campoMinado[linha+1][coluna+1] != CODIGO_BOMBA) buttonActionPerformed(this.buttons[linha+1][coluna+1], linha+1, coluna+1);
        if (linha+1 < LINHAS
                && this.campoMinado[linha+1][coluna] != CODIGO_BOMBA) buttonActionPerformed(this.buttons[linha+1][coluna], linha+1, coluna);
        if (linha+1 < LINHAS && coluna-1 >= 0
                && this.campoMinado[linha+1][coluna-1] != CODIGO_BOMBA) buttonActionPerformed(this.buttons[linha+1][coluna-1], linha+1, coluna-1);

        if (coluna-1 >= 0
                && this.campoMinado[linha][coluna-1] != CODIGO_BOMBA) buttonActionPerformed(this.buttons[linha][coluna-1], linha, coluna-1);
    }

    private void printCampoMinado() {
        for (int i = 0; i < LINHAS; i++) {
            for (int j = 0; j < COLUNAS; j++) {
                if (this.campoMinado[i][j] == CODIGO_BOMBA) {
                    System.out.print("* ");
                }
                else {
                    System.out.print(this.campoMinado[i][j] + " ");
                }
            }
            System.out.println("");
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldNomeP1 = new javax.swing.JTextField();
        jTextFieldNomeP2 = new javax.swing.JTextField();
        jLabelQuantasFaltam = new javax.swing.JLabel();
        jLabelBandeiraP1 = new javax.swing.JLabel();
        jLabelBandeiraP2 = new javax.swing.JLabel();
        jLabelBombasP1 = new javax.swing.JLabel();
        jLabelBombasP2 = new javax.swing.JLabel();
        jLabelVencedor = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Campo minado do Zé!");
        setExtendedState(getExtendedState()|JFrame.MAXIMIZED_BOTH);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setForeground(java.awt.Color.blue);
        jLabel1.setText("Jogador 1:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 14, -1, -1));

        jLabel2.setForeground(java.awt.Color.red);
        jLabel2.setText("Jogador 2:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 52, -1, -1));

        jTextFieldNomeP1.setText("Jogador 1");
        jPanel1.add(jTextFieldNomeP1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 11, 89, -1));

        jTextFieldNomeP2.setText("Jogador 2");
        jPanel1.add(jTextFieldNomeP2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 49, 89, -1));

        jLabelQuantasFaltam.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabelQuantasFaltam.setText("99");
        jPanel1.add(jLabelQuantasFaltam, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 20, -1, -1));
        jPanel1.add(jLabelBandeiraP1, new org.netbeans.lib.awtextra.AbsoluteConstraints(173, 11, -1, -1));
        jPanel1.add(jLabelBandeiraP2, new org.netbeans.lib.awtextra.AbsoluteConstraints(172, 49, -1, -1));

        jLabelBombasP1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelBombasP1.setForeground(java.awt.Color.blue);
        jLabelBombasP1.setText("0");
        jPanel1.add(jLabelBombasP1, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 10, -1, -1));

        jLabelBombasP2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelBombasP2.setForeground(java.awt.Color.red);
        jLabelBombasP2.setText("0");
        jPanel1.add(jLabelBombasP2, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 50, -1, -1));

        jLabelVencedor.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelVencedor.setText("Já temos um vencedor!!! Parabéns ");
        jPanel1.add(jLabelVencedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 30, -1, -1));

        jPanel2.setLayout(new java.awt.GridLayout(16, 16));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 656, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 656, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFramePrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabelBandeiraP1;
    private javax.swing.JLabel jLabelBandeiraP2;
    private javax.swing.JLabel jLabelBombasP1;
    private javax.swing.JLabel jLabelBombasP2;
    private javax.swing.JLabel jLabelQuantasFaltam;
    private javax.swing.JLabel jLabelVencedor;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField jTextFieldNomeP1;
    private javax.swing.JTextField jTextFieldNomeP2;
    // End of variables declaration//GEN-END:variables

    private void vezDoJogador(int jogador) {
        if (jogador == 1) {
            jLabelBandeiraP1.setVisible(true);
            jLabelBandeiraP2.setVisible(false);
        } else {
            jLabelBandeiraP2.setVisible(true);
            jLabelBandeiraP1.setVisible(false);
        }
    }

    private void vezDoProximoJogador() {
        if (this.jogadorDaVez == 1) {
            this.jogadorDaVez = 2;
        } else {
            this.jogadorDaVez = 1;
        }

        vezDoJogador(this.jogadorDaVez);
    }

    private boolean alguemVenceu() {
        return (this.totalBombasP1 > this.totalBombasJogo + this.totalBombasP2) ||
                (this.totalBombasP2 > this.totalBombasJogo + this.totalBombasP1);
    }

    private int quemVenceu() {
        int vencedor = 0;
        if (this.totalBombasP1 > this.totalBombasP2) vencedor = 1;
        else vencedor = 2;

        return vencedor;
    }

}
