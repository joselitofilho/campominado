/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package campominado;

import javax.swing.JButton;

/**
 *
 * @author joselito
 */
public class CampoJButton extends JButton {

    public CampoJButton() {
        super();
        setSize(16, 16);
    }

    public CampoJButton(String strText) {
        super(strText);
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
    }

}
